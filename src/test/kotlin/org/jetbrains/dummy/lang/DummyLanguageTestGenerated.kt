package org.jetbrains.dummy.lang

import org.junit.Test

class DummyLanguageTestGenerated : AbstractDummyLanguageTest() {
    @Test
    fun testInitElseOnly() {
        doTest("testData/initElseOnly.dummy")
    }
    
    @Test
    fun testInitThenOnly() {
        doTest("testData/initThenOnly.dummy")
    }
    
    @Test
    fun testInitThenReturnElse() {
        doTest("testData/initThenReturnElse.dummy")
    }
    
    @Test
    fun testRecursiveBinding() {
        doTest("testData/recursiveBinding.dummy")
    }
    
    @Test
    fun testRedeclaration() {
        doTest("testData/redeclaration.dummy")
    }
    
    @Test
    fun testRedeclarationAfterAssignment() {
        doTest("testData/redeclarationAfterAssignment.dummy")
    }
    
    @Test
    fun testRedeclarationAndReturn() {
        doTest("testData/redeclarationAndReturn.dummy")
    }
    
    @Test
    fun testReturnInBothBranches() {
        doTest("testData/returnInBothBranches.dummy")
    }
    
    @Test
    fun testReturnThenInitElse() {
        doTest("testData/returnThenInitElse.dummy")
    }
    
    @Test
    fun testSeveralUnreachableStatements() {
        doTest("testData/severalUnreachableStatements.dummy")
    }
    
    @Test
    fun testSimpleBad() {
        doTest("testData/simpleBad.dummy")
    }
    
    @Test
    fun testSimpleGood() {
        doTest("testData/simpleGood.dummy")
    }
    
    @Test
    fun testUndefinedFunction() {
        doTest("testData/undefinedFunction.dummy")
    }
}
