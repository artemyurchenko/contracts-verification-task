package org.jetbrains.dummy.lang

import org.jetbrains.dummy.lang.tree.*

class UnreachableCodeChecker(private val reporter: DiagnosticReporter) : AbstractChecker() {
    override fun inspect(file: File) {
        file.accept(UnreachableCodeVisitor(), Unit)
    }

    private fun reportUnreachableCode(stmt: Statement) {
        reporter.report(stmt, "The statement is unreachable")
    }

    private inner class UnreachableCodeVisitor : DummyLangVisitor<Boolean, Unit>() {
        override fun visitElement(element: Element, data: Unit): Boolean {
            element.acceptChildren(this, Unit)
            return false
        }

        override fun visitBlock(block: Block, data: Unit): Boolean {
            var returned = false
            for (stmt in block.statements) {
                if (returned) {
                    reportUnreachableCode(stmt)
                } else {
                    returned = stmt.accept(this, Unit)
                }
            }
            return returned
        }

        override fun visitIfStatement(ifStatement: IfStatement, data: Unit): Boolean {
            ifStatement.condition.accept(this, Unit)
            val thenReturned = ifStatement.thenBlock.accept(this, Unit)
            val elseReturned = ifStatement.elseBlock?.accept(this, Unit) ?: false
            return thenReturned && elseReturned
        }

        override fun visitReturnStatement(returnStatement: ReturnStatement, data: Unit): Boolean {
            returnStatement.acceptChildren(this, Unit)
            return true
        }
    }
}

